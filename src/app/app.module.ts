import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormsModule} from '@angular/forms'
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';


import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { TodosComponent } from './todos/todos.component';
import { TodoComponent } from './todo/todo.component';
import { RegistrationComponent } from './registration/registration.component';
import { CodesComponent } from './codes/codes.component';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
//firabace moodul-3 הבאים
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {environment} from '../environments/environment';
import { UsertodosComponent } from './usertodos/usertodos.component';// קישור לאינווירמנט
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    TodosComponent,
    TodoComponent,
    RegistrationComponent,
    CodesComponent,
    LoginComponent,
    UsertodosComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,//להתחברות
    //מגדירים ראוטים, כל ראוטים הם ג'ייסון
    AngularFireModule.initializeApp(environment.firebase), //חיבור לDB
    AngularFireDatabaseModule,//גם חיבור
   AngularFireAuthModule,//גם חיבור
   MatSelectModule,

    RouterModule.forRoot([
    {path:'',component:TodosComponent}, //מכיל יו-אר-אל אם הוא ריק מדובר באינדקס בקומפוננט נרשום את הקומפוננטים שמשתנים
    {path:'register',component:RegistrationComponent},
    {path:'codes',component:CodesComponent},
    {path:'login',component:LoginComponent},  
    {path:'usertodos',component:UsertodosComponent},
    {path:'**',component:TodosComponent},
  
    ])
  ],
 
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
