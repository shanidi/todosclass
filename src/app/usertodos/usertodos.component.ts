import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database'; //מוסיפים לדטהבייס

@Component({
  selector: 'usertodos',
  templateUrl: './usertodos.component.html',
  styleUrls: ['./usertodos.component.css']
})
export class UsertodosComponent implements OnInit {
  user='jack'
  name
 
  todoTextFromTodo="no text"

  showText($event){
    this.todoTextFromTodo = $event
  }

  changeuser(){
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe(  // האזנה לטודוז והרשמה
      todos => { // טודוז בפיירבייס
        this.todos=[]; // איפוס לטודוז שלנו
        todos.forEach ( // לכל טודו שיש שם
          todo=> { //טודו
            let y=todo.payload.toJSON();// יצירת ואיי וקליטת הנתונים אליו (פיילוד) עם המרתם לגייסון כי הם מגיעים כסטרינג
            y["$key"]=todo.key;// מוסיים לתכונות הקיימות את התכונה קי
            this.todos.push(y); //המערך שלנו- דיס
          }
        )
      }
    )

  }

  todos = [];

  /** מערך ידני מקומי
  todos = [{"text":"Study Json", "id":1},
          {"text":"Do final Project", "id":2},
          {"text":"Do do", "id":3}]
*/

  constructor(private db:AngularFireDatabase) //יוצר אובייקט בשם DB שיש לו פונקציות שמאפשרות לנו להתחבר לפיירבייס
   { } 

  ngOnInit() { //בטעינת הדף
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe(  // האזנה לטודוז והרשמה
      todos => { // טודוז בפיירבייס
        this.todos=[]; // איפוס לטודוז שלנו
        todos.forEach ( // לכל טודו שיש שם
          todo=> { //טודו
            let y=todo.payload.toJSON();// יצירת ואיי וקליטת הנתונים אליו (פיילוד) עם המרתם לגייסון כי הם מגיעים כסטרינג
            y["$key"]=todo.key;// מוסיים לתכונות הקיימות את התכונה קי
            this.todos.push(y); //המערך שלנו- דיס
          }
        )
      }
    )
  }

}
