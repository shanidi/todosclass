import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database'; //מוסיפים לדטהבייס
import {AngularFireList} from '@angular/fire/database'; // מוסיפים לדטה בייס
import {AuthService} from '../auth.service';//נחוץ כי רק באמצעותו אפשר להגיכ ליוזר ID
import { TodosService } from '../todos.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todoTextFromTodo="no text"
  todos = [];
  text; //חייב את התכונה שנוכל לעבוד עם אן גי מודל
  name;

  showText($event){
    this.todoTextFromTodo = $event
    this.name=this.todoTextFromTodo;
  }


  
addtodo(text:string){
  this.todosService.addTodo(this.text); //שולח לסרביס
  this.text='';// מאוקן את התיבה
  }
  

  

  /** מערך ידני מקומי
  todos = [{"text":"Study Json", "id":1},
          {"text":"Do final Project", "id":2},
          {"text":"Do do", "id":3}]
*/

  constructor(private db:AngularFireDatabase, 
    private authService:AuthService,
    private todosService:TodosService) //יוצר אובייקט בשם DB שיש לו פונקציות שמאפשרות לנו להתחבר לפיירבייס
   { } 
   


  ngOnInit() { //בטעינת הדף
    this.authService.user.subscribe(user => { //סובסקרייס מקבלת ארור פנקשן תמיד. מה שלפני החץ בפונקציה זה הקלט, מה שבתוך הסוגריים המסולסלים זה גוף הפונקציה
      user.uid   
      this.db.list('/user/'+user.uid+'/todos').snapshotChanges().subscribe(  // האזנה לטודוז והרשמה
      todos => { // טודוז בפיירבייס
        this.todos=[]; // איפוס לטודוז שלנו
        todos.forEach ( // לכל טודו שיש שם
          todo=> { //טודו
            let y=todo.payload.toJSON();// יצירת ואיי וקליטת הנתונים אליו (פיילוד) עם המרתם לגייסון כי הם מגיעים כסטרינג
            y["$key"]=todo.key;// מוסיים לתכונות הקיימות את התכונה קי
            this.todos.push(y); //המערך שלנו- דיס
          }
        )
      }
    )
   })
 
    
  }

}
