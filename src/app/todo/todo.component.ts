import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { flush } from '@angular/core/testing';
import { TodosService } from '../todos.service';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})


export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();
  text;
  key;
  id;
  showEditField=false;
  temptext;
  showTheButton=false;
 
  send(){
    console.log('event caught')
    this.myButtonClicked.emit(this.text);
  }

  showButton(){
    this.showTheButton = true;
  }
  
  hideButton(){
    this.showTheButton = false
  }

  constructor(private todosService:TodosService) { }
  ngOnInit() {
    this.text = this.data.text; //מסתמכים על זה שיש שדה בשם
    this.key=this.data.$key; //מערדכנת את הפרמטר קי
    //דטה-מוגדר באינפוט

  }
  delete (){
   this.todosService.delete(this.key) 
  }
  

  showEdit(){
    this.showEditField=true;
    this.temptext = this.text;
  }

  cancel(){
    this.showEditField=false;
    this.text = this.temptext;
  }


  save(){
    this.todosService.update(this.key ,this.text)//שליחה לסרביס
    this.showEditField=false; //שינוי לפולס
  }

}

 /*

  ngOnInit() //כל אלמנט טודו שנוצר מפעיל את הפונקציה
  {
    this.text= this.data.text; //קלטת הטקסט
    this.id= this.data.id;//העברה של איידי
  }
*/
