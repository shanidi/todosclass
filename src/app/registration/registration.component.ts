import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from "@angular/router";



@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {

  email: string;
  password: string;
  name: string;
  error;


  signup(){//פרומיס- חד פעמי, מהסיבה הזו צריך דאן 
    // console.log("sign up clicked" +' '+ this.email+' '+this.password+' '+this.name)
     this.authService.signup(this.email,this.password)
       .then(value=>{ //וליו זה משתנה שאנחנו מקבלים בחזרה, חשוב לזכור לכתוב דאן כי הקוד עובד בצורה א-סינכרונית. הקוד ממשיך לרוץ גם אם השורה שלפני עוד לא מומשה
          this.authService.updateProfile(value.user,this.name);//לוליו הזה נוסיף את השם
          this.authService.addUser(value.user,this.name); // כפילות כי אי אפשר להוסיך אנדפויינט בלי ערך
        }).then(value=>{
         this.router.navigate(['/']);//העברה לרשימת המשימות
       }).catch(err => { 
         this.error = err;//שמירת המשתנה של השגיאה
         console.log(err);
       })
   }


  constructor(private authService:AuthService,private router:Router) { } //נרצה לייצור את התכונה גם במחלקה הזו

  ngOnInit() {
  }

}




