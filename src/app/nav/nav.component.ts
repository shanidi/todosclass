import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  
  eerror='';
  toRegister(){
  this.router.navigate(['/register']);//
  } 
  toCodes(){
    this.router.navigate(['/codes']);
  } 

logout(){
  this.authService.logout()
  .then (value=>{
    this.router.navigate(['/login'])
  }).catch(err=>{console.log(err)})
}

  constructor(private authService:AuthService,private router:Router) { } //הגדרנו אובייקט בתוך הקונסטקטור - מבצע הזרקת פניות- מה שאפשר לנו להשתמש באובייקט במחלקה

  ngOnInit() {
  }

}
